package com.example.gorinih.klibscroll

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.gorinih.klibscroll.аdapter.CreatorBodyAdapter
import com.example.gorinih.klibscroll.аdapter.CreatorHeadAdapter
import com.example.gorinih.klibscroll.interfaces.CreatorBodyItems
import com.example.gorinih.klibscroll.interfaces.CreatorHeadItems
import kotlinx.android.synthetic.main.layout_scrollable_list.view.creatorScrollableListBody
import kotlinx.android.synthetic.main.layout_scrollable_list.view.creatorScrollableListHead
import kotlinx.android.synthetic.main.layout_scrollable_list_edit.view.editScrollableListHead
import kotlinx.android.synthetic.main.layout_scrollable_list_edit.view.editScrollableListBody

class CreateScrollableList constructor(context: Context, attrs: AttributeSet) :
    FrameLayout(context, attrs) {

    //адаптеры Head и Body инициализируются в методе setCreatorViewScrollableList
    private lateinit var creatorHeadAdapter: CreatorHeadAdapter
    private lateinit var creatorBodyAdapter: CreatorBodyAdapter

    //изначально при создании первые элементы Head и Body = 0
    private var previousSnapPositionHead: Int = 0
    private var previousSnapPositionBody: Int = 0

    /*прослушиватель скролинга в заголовке или теле компонента
    * если компоненты изменились то в зависимости заголовок или тело изменяем положение
    * элементов в другом соответственно
    */
    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            if (recyclerView.scrollState == RecyclerView.SCROLL_STATE_IDLE) { //если было смещение элементов
                val newPosition =
                    getPositionCurrentCell(recyclerView) //получаем текущий номер элемента
                when (recyclerView) { // в зависимости какой список листался изменяем другой
                    creatorScrollableListHead -> { // если изменили Head
                        Log.d(
                            "Snap",
                            "HEAD предыдущая позиция $previousSnapPositionBody текущая $newPosition "
                        )
                        if (newPosition != previousSnapPositionHead) { //если было изменение в Head то сдвигаем элементы в Body
                            previousSnapPositionHead =
                                newPosition //изменяем номер элемента на текущий
                            val positionBody =
                                creatorHeadAdapter.getNewPositionHead(newPosition) //получаем первый номер элемента Body для текущего элемента Head
                            if (positionBody != previousSnapPositionBody) { //двигаем Body если есть необходимость
                                previousSnapPositionBody = positionBody
                                creatorScrollableListBody.smoothScrollToPosition(positionBody)
                            }
                        }
                    }
                    creatorScrollableListBody -> { //если изменили Body
                        Log.d(
                            "Snap",
                            "BODY предыдущая позиция $previousSnapPositionBody текущая $newPosition "
                        )
                        if (newPosition != previousSnapPositionBody) {
                            previousSnapPositionBody = newPosition
                            val positionHead =
                                creatorBodyAdapter.getNewPositionBody(newPosition)
                            if (positionHead != previousSnapPositionHead) {
                                previousSnapPositionHead = positionHead
                                creatorScrollableListHead.smoothScrollToPosition(positionHead)
                            }
                        }
                    }
                }
            }
        }
    }

    init { //инициализация модуля
        if (isInEditMode) {
            setUpEditMode() //если в редакторе то визуализируем компонент
        } else
            setUpScrollableList() //если запущен в рабочем режиме создаем компонент
    }

    //получение номера позиции текущего элемента в Body или Head
    private fun getPositionCurrentCell(recyclerView: RecyclerView): Int {
        return SnapHelperHorisont().getSnapPosition(recyclerView)
    }


    private fun setUpEditMode() { // визуализируем в редакторе
        View.inflate(context, R.layout.layout_scrollable_list_edit, this)
        val bodyItemHeight = resources.getDimension(R.dimen.dimens_48dp)
        val headItemHeight = resources.getDimension(R.dimen.dimens_60dp)
        val bodyItemWidth = resources.getDimension(R.dimen.dimens_93dp)
        val headItemWidth = resources.getDimension(R.dimen.dimens_132dp)

        for (i in 0..3) {
            val headCell = LayoutInflater.from(context).inflate(
                R.layout.head_cell_edit, editScrollableListHead, false
            )
            val bodyCell = LayoutInflater.from(context).inflate(
                R.layout.body_cell_edit, editScrollableListBody, false
            )
            val headMarginLayoutParams =
                MarginLayoutParams(headItemWidth.toInt(), headItemHeight.toInt())
            val bodyMarginLayoutParams =
                MarginLayoutParams(bodyItemWidth.toInt(), bodyItemHeight.toInt())
            bodyMarginLayoutParams.leftMargin = resources.getDimension(R.dimen.dimens_24dp).toInt()
            headMarginLayoutParams.leftMargin = resources.getDimension(R.dimen.dimens_32dp).toInt()

            editScrollableListHead.addView(headCell, headMarginLayoutParams)
            editScrollableListBody.addView(bodyCell, bodyMarginLayoutParams)
        }
    }


    private fun setUpScrollableList() { //создаем рабочий режим
        View.inflate(context, R.layout.layout_scrollable_list, this)
        //удаляем слушатели на Head и Body
        creatorScrollableListHead.removeOnScrollListener(scrollListener)
        creatorScrollableListBody.removeOnScrollListener(scrollListener)

    }

    // основной метод который вызывает клиент передавая соответствующие интерфейсы Head и Body
    fun setCreatorViewScrollableList(
        creatorHeadItems: CreatorHeadItems,
        creatorBodyItems: CreatorBodyItems
    ) {
        creatorHeadAdapter = CreatorHeadAdapter(creatorHeadItems)
        creatorBodyAdapter = CreatorBodyAdapter(creatorBodyItems)

        creatorScrollableListHead.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        creatorScrollableListBody.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        creatorScrollableListHead.adapter = creatorHeadAdapter
        creatorScrollableListBody.adapter = creatorBodyAdapter

        val positionSnapHelperHead = SnapHelperHorisont()
        val positionSnapHelperBody = SnapHelperHorisont()
        positionSnapHelperHead.attachToRecyclerView(creatorScrollableListHead)
        positionSnapHelperBody.attachToRecyclerView(creatorScrollableListBody)

        creatorScrollableListHead.addOnScrollListener(scrollListener)
        creatorScrollableListBody.addOnScrollListener(scrollListener)
    }
}