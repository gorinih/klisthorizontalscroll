package com.example.gorinih.klibscroll.аdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.gorinih.klibscroll.аdapter.CreatorHeadAdapter.HeadViewHolder
import com.example.gorinih.klibscroll.R
import com.example.gorinih.klibscroll.interfaces.CreatorHeadItems

class CreatorHeadAdapter(creatorHeadAdapters: CreatorHeadItems) : Adapter<HeadViewHolder>() {

    private val creatorHeadAdapter = creatorHeadAdapters

    //создаем  и возвращаем ViewHolder Head
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeadViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.head_cell, parent, false
        )
        return HeadViewHolder(itemView)
    }

    //возвращаем количество элементов в Head
    override fun getItemCount(): Int = creatorHeadAdapter.getCountHead()

    //строим ViewHolder Head
    override fun onBindViewHolder(holder: HeadViewHolder, position: Int) {
        holder.bind(position, creatorHeadAdapter)
    }

    //возвращаем номер первого элемена Body соответствующего указанному в параметре (position) номеру Head
    fun getNewPositionHead(position: Int): Int = creatorHeadAdapter.getNewPositionHead(position)

    //собираем ViewHolder
    class HeadViewHolder(itemView: View) : ViewHolder(itemView) {
        fun bind(
            position: Int,
            creatorHeadAdapter: CreatorHeadItems
        ) {
            val container = itemView as ViewGroup
            val itemView = creatorHeadAdapter.instantiateItemHead(
                LayoutInflater.from(itemView.context), container, position
            )
            container.removeAllViews()
            container.addView(itemView)
            this.itemView.requestLayout()
        }

    }
}